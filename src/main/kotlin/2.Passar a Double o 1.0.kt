import java.lang.NumberFormatException
import java.util.*

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/03/16
* TITLE: Pasar a Double o 1.0
*/
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    val num = scanner.next()

    println(aDoubleoU(num))
}
fun aDoubleoU(num: String): Double {
    return try {
        num.toDouble()
    } catch (e: NumberFormatException) {
        1.0
    }
}