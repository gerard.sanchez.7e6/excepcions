
import java.lang.IndexOutOfBoundsException
import java.util.*
import kotlin.system.exitProcess

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/03/16
* TITLE: Aplicació lliure
*/


data class Beguda(
    val nom : String,
    var preu: Double,
    val saludable: Boolean)

fun main() {


    val list = mutableListOf<Beguda>()
    list.add(Beguda("Aigua", 1.50, true))
    list.add(Beguda("Cocacola", 2.10, false))
    list.add(Beguda("Ron", 6.45, false))

    val scanner = Scanner(System.`in`)
    println("Quin producte vols?")

    for (i in 0..list.lastIndex) {
        println("${i + 1} -> ${list[i].nom}")
    }

    val userNumber = scanner.nextInt()
    searchProduct(list,userNumber)

    println("Nou preu:")
    val newPrice = scanner.next()

    updatePrice(list, userNumber,newPrice)

}
fun searchProduct(list : MutableList<Beguda>, userNumber: Int ) {
    try{
        println("${list[userNumber-1].nom} / ${list[userNumber-1].preu}€ / ${list[userNumber-1].saludable}")
    }catch (e: IndexOutOfBoundsException){
        println("Error: No existeix")
        exitProcess(0)
    }

}
fun updatePrice(list : MutableList<Beguda>, userNumber: Int, newPrice : String){

    try{
        list[userNumber-1].preu = newPrice.toDouble()
        println("Preu final: ${list[userNumber-1].nom} / ${list[userNumber-1].preu}€ / ${list[userNumber-1].saludable}")
    }catch (e: IllegalArgumentException) {
        println("Error: Pàrametre no vàlid")
    }
}

