import java.io.File
import java.io.IOException

import java.util.*

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/03/16
* TITLE: Llegir un fitxer
*/
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix el la ruta del fitxer:")
    val userFile = scanner.next()

    println(fileFind(userFile))
}
fun fileFind(userFile: String): String {
 val file = File(userFile)

    return try {
       file.readLines().toString()
    } catch (e: IOException) {
        "${e.message}"
    }
}