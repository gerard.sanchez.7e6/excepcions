import java.util.*

/*
* AUTHOR: Gerard Sanchez Preto
* DATE: 23/03/16
* TITLE: Divideix o Cero
*/
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix el dividend:")
    val num1 = scanner.nextInt()

    println("Introdueix el divisor:")
    val num2 = scanner.nextInt()

    println(dividir(num1,num2))
}
fun dividir(num1: Int, num2: Int): Int {
    var resultat = 0
    try {
        resultat = num1 / num2
    } catch (e: ArithmeticException) {
        return 0
    }
    return resultat
}


